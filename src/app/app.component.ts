import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Angular';
  months = ['January', 'Feburary', 'March', 'April', 'May',
            'June', 'July', 'August', 'September',
            'October', 'November', 'December'];
  httpdata;
  changemonths(event) {
    alert('Changed month from the Dropdown');
    console.log(event);
  }
  constructor(private http: Http) { }
  ngOnInit() {
    this.http.get('http://jsonplaceholder.typicode.com/users')
      .pipe(
        map(res => res.json()) // or any other operator
      )
      .subscribe(data => this.displaydata(data));
  }
  displaydata(data) {
    this.httpdata = data;
  }


}
